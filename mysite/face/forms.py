from django import forms
from django.forms.widgets import ClearableFileInput
from .models import StudentImages

class StudentImagesForm(forms.ModelForm):
    images = forms.ImageField(widget=forms.ClearableFileInput(attrs={'multiple': True}))

    class Meta:
        model = StudentImages
        fields = ['images']