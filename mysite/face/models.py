from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
 
# Create your models here.
class Student(User,models.Model):
    auto_id = models.AutoField(primary_key=True)
    student_code = models.CharField(max_length=10, blank=False, unique=True, default=None)
    full_name = models.CharField(max_length=50, blank=False, null=True)
    class_name = models.CharField(max_length=50, blank=False, null=True)
    birth_date = models.DateField(null=True)
    phone = models.CharField(max_length=11, null=True)
    address =models.CharField(max_length=100, null=True)
    avatar = models.ImageField(null=True)
    created_at = models.DateTimeField(auto_now=True,null=False)
    created_by = models.CharField(max_length=50, blank=False, null=True)
    updated_at = models.DateTimeField(blank=False, null=True)
    updated_by = models.CharField(max_length=50, blank=False, null=True)
    def __str__(self):
        return self.student_code + ' - ' + self.full_name
    @property
    def imageUrl(self):
        try:
            url = self.avatar.url
        except:
            url = '/'
        return url
    @property
    def birth_dateFm(self):
        try:
            #parse date to dd/mm/yyyy
            dateFormated = self.birth_date.strftime('%d/%m/%Y')
        except:
            dateFormated = self.birth_date
        return dateFormated
    
    def serializable(self):
        return {
            'auto_id': self.auto_id,
            'student_code': self.student_code,
            'full_name': self.full_name,
            'class_name': self.class_name,
            'birth_date': self.birth_dateFm,
            'phone': self.phone,
            'email': self.email,
            'address': self.address,
            'avatar': self.imageUrl
        }
    class Meta:
        verbose_name_plural = 'Students'

class Teacher(User,models.Model):
    auto_id = models.AutoField(primary_key=True)
    teacher_code = models.CharField(max_length=10, blank=False, unique=True, default=None)
    full_name = models.CharField(max_length=50, blank=False, null=True)
    birth_date = models.DateField(null=True)
    phone = models.CharField(max_length=11, null=True)
    address = models.CharField(max_length=100, null=True)
    avatar = models.CharField(max_length=100, null=True)
    faculty = models.CharField(max_length=50, blank=False, null=True)
    department = models.CharField(max_length=50, blank=False, null=True)
    created_at = models.DateTimeField(auto_now=True,null=False)
    created_by = models.CharField(max_length=50, blank=False, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    updated_by = models.CharField(max_length=50, blank=True, null=True)
    def __str__(self):
        return self.teacher_code + ' - ' + self.full_name
    @property
    def imageUrl(self):
        try:
            url = self.avatar
        except:
            url = '/'
        return url
    @property
    def birth_dateFm(self):
        try:
            #parse date to dd/mm/yyyy
            dateFormated = self.birth_date.strftime('%d/%m/%Y')
        except:
            dateFormated = self.birth_date
        return dateFormated
    
class StudentImages(models.Model):
    auto_id = models.AutoField(primary_key=True)
    student_code = models.ForeignKey(Student, on_delete=models.CASCADE, to_field="auto_id")
    images = models.ImageField()
    created_at = models.DateTimeField(auto_now=True,null=False)
    created_by = models.CharField(max_length=50, blank=False, null=True)
    updated_at = models.DateTimeField(blank=False, null=True)
    updated_by = models.CharField(max_length=50, blank=False, null=True)
    def __str__(self):
        return self.student_code.full_name + " - " + self.images.name
    
class SubjectClass(models.Model):
    auto_id = models.AutoField(primary_key=True)
    class_code = models.CharField(max_length=30, unique=True)
    ma_hoc_phan = models.CharField(max_length=50, blank=False, null=True)
    class_name = models.CharField(max_length=50, blank=False, null=True)
    hoc_ky = models.CharField(max_length=50, blank=False, null=True)
    nam_hoc = models.CharField(max_length=50, blank=False, null=True)
    created_at = models.DateTimeField(auto_now=True,null=False)
    created_by = models.CharField(max_length=50, blank=False, null=True)
    updated_at = models.DateTimeField(blank=False, null=True)
    updated_by = models.CharField(max_length=50, blank=False, null=True)
    def __str__(self):
        return self.class_name
    
    def serializable(self):
        return {
            'auto_id': self.auto_id,
            'class_code': self.class_code,
            'ma_hoc_phan': self.ma_hoc_phan,
            'class_name': self.class_name,
            'hoc_ky': self.hoc_ky,
            'nam_hoc': self.nam_hoc
        }
    
class StudentInSubjectClass(models.Model):
    auto_id = models.AutoField(primary_key=True)
    student_code = models.ForeignKey(Student, on_delete=models.CASCADE, to_field="auto_id")
    class_code = models.ForeignKey(SubjectClass, on_delete=models.CASCADE, to_field="auto_id")
    created_at = models.DateTimeField(auto_now=True,null=False)
    created_by = models.CharField(max_length=50, blank=False, null=True)
    updated_at = models.DateTimeField(blank=False, null=True)
    updated_by = models.CharField(max_length=50, blank=False, null=True)
    class Meta:
        unique_together = (("student_code", "class_code"),)
    def __str__(self):
        return self.student_code.full_name + " - " + self.class_code.class_name
    def serializable(self):
        return {
            'auto_id': self.auto_id,
            'student_code': self.student_code.serializable(),
            'class_code': self.class_code.serializable()
        }
    
class Attendance(models.Model):
    auto_id = models.AutoField(primary_key=True)
    date_time = models.DateTimeField(null=True)
    student_code = models.ForeignKey(Student, on_delete=models.CASCADE, to_field="auto_id",null=True)
    is_present = models.CharField(max_length=1, blank=False, null=False, default='0')
    subject_class = models.ForeignKey(SubjectClass, on_delete=models.CASCADE, to_field="auto_id", null=True)
    created_at = models.DateTimeField(auto_now=True,null=False)
    created_by = models.CharField(max_length=50, blank=False, null=True)
    updated_at = models.DateTimeField(blank=False, null=True)
    updated_by = models.CharField(max_length=50, blank=False, null=True)
    imagePath = models.CharField(max_length=100, blank=False, null=True)
    class Meta: 
        unique_together = (("student_code", "subject_class","date_time"),)
    def __str__(self):
        return self.student_code.full_name + " - " +\
            self.subject_class.class_name + " - " + self.date_time.strftime("%d/%m/%Y, %H:%M:%S") + " - " + self.is_present
    def serializable(self):
        return {
            'date_time': self.date_time.strftime("%d/%m/%Y, %H:%M:%S"),
            'student_code': self.student_code.serializable(),
            'is_present': self.is_present,
            'subject_class': self.subject_class.serializable(),
            'imagePath': self.imagePath
        }
        
class Learning(models.Model):
    auto_id = models.AutoField(primary_key=True)
    subject_class = models.ForeignKey(SubjectClass, on_delete=models.CASCADE, to_field="auto_id", null=True)
    title = models.CharField(max_length=100, blank=False, null=True)
    content = models.TextField(blank=False, null=True)
    created_at = models.DateTimeField(auto_now=True,null=False)
    created_by = models.CharField(max_length=50, blank=False, null=True)
    updated_at = models.DateTimeField(blank=False, null=True)
    updated_by = models.CharField(max_length=50, blank=False, null=True)
    
    def __str__(self):
        return self.title
    
    def main_data(self):
        return {
            'auto_id': self.auto_id,
            'title': self.title,
            'content': self.content,
        }
        
class PathFileLearning(models.Model):
    auto_id = models.AutoField(primary_key=True)
    learning = models.ForeignKey(Learning, on_delete=models.CASCADE, to_field="auto_id", null=True)
    name = models.CharField(max_length=200, blank=False, null=True)
    path = models.CharField(max_length=200, blank=False, null=True)
    created_at = models.DateTimeField(auto_now=True,null=False)
    created_by = models.CharField(max_length=50, blank=False, null=True)
    updated_at = models.DateTimeField(blank=False, null=True)
    updated_by = models.CharField(max_length=50, blank=False, null=True)
    
    def __str__(self):
        return self.path
        

    
    