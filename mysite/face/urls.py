from django.urls import path
from face import views as app_views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', app_views.home, name='home'),
    path('face', app_views.face, name='face'),
    path('api/face', app_views.faceApi, name='face_api'),
    path('students', app_views.students, name='students'),
    path('login', app_views.my_login, name='login'),
    path('logout', app_views.my_logout, name='logout'),
    path('students/update', app_views.student_update, name='student_update'),
    path('students/create', app_views.student_create, name='student_create'),
    path('students/multiple-create', app_views.student_multiple_create, name='student_multiple_create'),
    path('students/<str:student_code>', app_views.student_detail, name='student_detail'),
    path('students/<str:student_code>/update', app_views.student_update, name='student_update'),
    path('students/<str:student_code>/delete', app_views.student_delete, name='student_delete'),
    path('students/<str:student_code>/images', app_views.studentImages, name='student_images'),
    path('students/<str:student_code>/video', app_views.video, name='student_video'),
    path('students/<str:student_code>/images/delete', app_views.studentImages_delete, name='student_images_delete'),
    path('classes', app_views.classes, name='classes'),
    path('classes/create', app_views.classes_create, name='classes_create'),
    path('classes/<str:class_code>/upadte', app_views.classes_update, name='classes_update'),
    path('classes/<str:class_code>/details', app_views.classes_details, name='classes_details'),
    path('classes/delete', app_views.classes_delete, name='classes_delete'),
    path('classes/<str:class_code>/attendances', app_views.class_attendance, name='hoc_tap_va_diem_danh'),
    path('forgot-password', app_views.forgotPassword, name='forgot_password'),
    path('change-password', app_views.change_password, name='change_password'),
    path('teachers', app_views.teachers, name='teachers'),
    path('teachers/create', app_views.teacher_create, name='teacher_create'),
    path('teachers/<str:teacher_code>/delete', app_views.teacher_delete, name='teacher_delete'),
    path('teachers/<str:teacher_code>/update', app_views.teacher_update, name='teacher_update'),
]
urlpatterns += static(settings.MEDIA_FACE_URL, document_root=settings.MEDIA_FACE)
