$(document).ready(function(){
    if($(window).width() < 768){
        $("#accordionSidebar").addClass("toggled");
    }
    // event windows resize
    $(window).resize(function(){
        if($(window).width() < 768){
            $("#accordionSidebar").addClass("toggled");
        }
    });
});