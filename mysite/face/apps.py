from django.apps import AppConfig
from django.db.models.signals import post_save

class FaceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'face'

    def ready(self):
        import face.signals # Thêm dòng này vào

        # Get the Student model class
        studentImage_model = self.get_model('StudentImages')
        student_model = self.get_model('Student')

        # Connect the create_student_folder() signal to the post_save signal
        post_save.connect(face.signals.create_student_folder_handler, sender=studentImage_model)
        post_save.connect(face.signals.create_student_folder2, sender=student_model)
        post_save.connect(face.signals.delete_old_image, sender=student_model)
        post_save.connect(face.signals.delete_old_image2, sender=studentImage_model)