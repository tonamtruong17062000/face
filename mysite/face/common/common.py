import cv2, io, os,random, string, uuid
import numpy as np
from django.conf import settings
from face.models import *
from unidecode import unidecode
from django.contrib.auth.models import *


def recognize_face(img_path):
    # Đọc ảnh và chuyển đổi sang ảnh xám
    img = cv2.imread(img_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    # Cân bằng độ sáng
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8)) # Tạo một bộ cân bằng histogram giới hạn
    equalized_image = clahe.apply(gray) # Áp dụng bộ cân bằng histogram giới hạn vào ảnh xám

    # Tải bộ phát hiện khuôn mặt đã được đào tạo trước
    face_cascade = cv2.CascadeClassifier(str(settings.BASE_DIR) + '/static/data/haarcascades/haarcascade_frontalface_alt2.xml')

    # Phát hiện các khuôn mặt trong ảnh
    faces = face_cascade.detectMultiScale(equalized_image, scaleFactor=1.1, minNeighbors=5, minSize=(100, 100))
    
    students = [] # Khởi tạo danh sách sinh viên
    
    # Lặp lại các khuôn mặt được phát hiện và cắt chúng thành các hình chữ nhật đều nhau
    for (x, y, w, h) in faces:
        roi_gray = equalized_image[y:y + h, x:x + w] # Cắt khuôn mặt
        resized_roi = cv2.resize(roi_gray, (100, 100))
        
        # Tải bộ nhận diện khuôn mặt đã được đào tạo trước
        recognizer = cv2.face.LBPHFaceRecognizer_create()
        recognizer.read(os.path.join(settings.BASE_DIR, "static", "data","train","trainer.yml"))
        
        # So sánh khuôn mặt đã cắt với các khuôn mặt đã được đào tạo trước
        id, confidence = recognizer.predict(resized_roi)
        
        # Nếu độ chính xác > 60, trả về thông tin sinh viên
        if confidence > 60 and confidence <= 97: 
            # Lấy thông tin sinh viên từ CSDL
            student = getStudentById(id)
            students.append(student)
            #Vẽ hình chữ nhật quanh khuôn mặt
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2) # Vẽ hình chữ nhật với màu red và độ dày 2px, tọa độ x, y, w, h
            cv2.putText(img, unidecode(student.full_name) + " - " + \
                str(confidence), (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2) # Ghi tên sinh viên và độ chính xác

        if confidence > 40 and confidence <= 60:
            student = getStudentById(id)
            students.append(student)
            #Vẽ hình chữ nhật quanh khuôn mặt
            cv2.rectangle(img, (x, y), (x + w, y + h), (0,130,250), 2) # Vẽ hình chữ nhật với màu cam và độ dày 2px, tọa độ x, y, w, h
            cv2.putText(img, unidecode(student.full_name) + " - " + \
                str(confidence), (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,130,250), 2) # Ghi tên sinh viên và độ chính xác
        if confidence >= 0 and confidence <= 40:
            student = getStudentById(id)
            students.append(student)
            cv2.rectangle(img, (x, y), (x + w, y + h), (0,255,0), 2) # Vẽ hình chữ nhật với màu xanh và độ dày 2px, tọa độ x, y, w, h
            cv2.putText(img, unidecode(student.full_name) + " - " + \
                str(confidence), (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 2) # Ghi tên sinh viên và độ chính xác
                
    # lưu ảnh đã nhận diện khuôn mặt, ghi đè lên ảnh gốc
    cv2.imwrite(img_path, img)
    newNames = img_path.split('\\')[-1]
    return newNames,students

def getStudentById(auto_id):
    student = Student.objects.filter(auto_id=auto_id).first()
    return student

def capture_frames(video_path, num_frames_per_second):
    
    #Kiểm tra path của video có tồn tại hay không
    if not os.path.exists(video_path):
        print('Video path not exist! ', video_path)
        return []
    
    video_capture = cv2.VideoCapture(video_path)
    # Lấy tốc độ khung hình của video
    fps = video_capture.get(cv2.CAP_PROP_FPS)
    
    # Lấy độ dài của video
    video_len = int(video_capture.get(cv2.CAP_PROP_FRAME_COUNT)) / fps
    # Làm tròn độ dài của video
    video_len = int(video_len)
    
    start_frame = int(1 * fps)
    end_frame = int(video_len * fps)
    interval = int(fps / num_frames_per_second)

    # Thiết lập khung hình ban đầu
    video_capture.set(cv2.CAP_PROP_POS_FRAMES, start_frame)

    frame_count = 0
    start_frame
    imgs = []

    while start_frame <= end_frame:
        ret, frame = video_capture.read()
        if not ret:
            break

        if frame_count % interval == 0:
           # Lưu khung lại khung hình với tên random
            img_path = 'media/face/temp/' + randomText(20) + '.jpg'
            issaved = cv2.imwrite(img_path, frame)
            if issaved:
                imgs.append(img_path)
        start_frame += 1
        frame_count += 1
    return imgs

def randomText(length):
    letters = string.ascii_letters + string.digits
    return ''.join(random.choice(letters) for i in range(length))