import os
from venv import logger
import cv2
import numpy as np
from django.conf import settings
from face.models import *

# TIỀN XỬ LÝ ẢNH
def preprocess_image(image_path):
    # Đọc ảnh màu và chuyển đổi sang ảnh xám
    image = cv2.imread(image_path)
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) # Chuyển đổi sang ảnh xám
    
    # Cân bằng histogram để cải thiện độ tương phản của ảnh
    # equalized_image = cv2.equalizeHist(gray_image) 
    
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8)) # Tạo một bộ cân bằng histogram giới hạn
    equalized_image = clahe.apply(gray_image) # Áp dụng bộ cân bằng histogram giới hạn vào ảnh xám
    
    # Nạp bộ nhận diện khuôn mặt và mắt
    face_cascade = cv2.CascadeClassifier(str(settings.BASE_DIR) + "/static/data/haarcascades/haarcascade_frontalface_alt2.xml")
    
    # Cắt ảnh để loại bỏ nền nhiễu và chỉ giữ lại khuôn mặt
    faces = face_cascade.detectMultiScale(equalized_image, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30)) #  tăng độ chính xác bằng cách tăng scaleFactor và minNeighbors
    
    for (x, y, w, h) in faces:
        roi = equalized_image[y:y+h, x:x+w] # Trich xuất khuôn mặt
        resized_roi = cv2.resize(roi, (100, 100)) # Cắt lại khuôn mặt và resize về kích thước 100x100
        return resized_roi
    return None

def generate_data():
    # Thư mục chứa các hình ảnh khuôn mặt
    faces_dir = str(settings.BASE_DIR) + "/static/images/"
    
    # Tạo danh sách các đường dẫn tới các hình ảnh khuôn mặt
    image_paths = []
    for root, dirs, files in os.walk(faces_dir):
            for dir in dirs:
                for subroot, subdirs, subfiles in os.walk(os.path.join(root, dir)):
                    for subfile in subfiles:
                        if subfile.endswith(".jpg") or subfile.endswith(".png"):
                            image_paths.append(os.path.join(subroot, subfile))
    
    # Tạo danh sách các khuôn mặt và nhãn tương ứng
    labels = []
    faces = []
    for image_path in image_paths:
        label = os.path.basename(os.path.dirname(image_path))
        resized_roi = preprocess_image(image_path)
        if resized_roi is not None:
            faces.append(resized_roi)
            labels.append(int(label))
            
            # Tăng cường dữ liệu bằng cách sử dụng phép đối xứng dọc
            faces.append(cv2.flip(resized_roi, 1)) # 1: dọc
            labels.append(int(label))
            
            
    return faces, np.array(labels)

def train():
    # Tạo một bộ phân loại khuôn mặt sử dụng thuật toán LBPH
    face_recognizer = cv2.face.LBPHFaceRecognizer_create()

    # Huấn luyện bộ phân loại với các hình ảnh khuôn mặt và nhãn tương ứng
    faces, labels = generate_data()
    face_recognizer.train(faces, labels)

    # Lưu mô hình vào file YAML
    face_recognizer.write(str(settings.BASE_DIR) + "/static/data/train/trainer.yml")
    
    return