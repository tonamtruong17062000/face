import django_tables2 as tables
from .models import Student

class StudentTable(tables.Table):
    class Meta:
        model = Student
        template_name = "django_tables2/bootstrap.html"
        fields = ("avatar","student_code", "full_name", "class_name", "phone", "Action")