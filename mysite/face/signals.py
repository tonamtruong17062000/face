import os
from django.conf import settings
from django.db.models.signals import post_save, pre_save, pre_delete, post_delete
from django.dispatch import receiver
from .models import StudentImages, Student, Teacher
from django.core.exceptions import ObjectDoesNotExist
import shutil

#Hàm tạo tên ảnh ngẫu nhiên 20 ký tự
def random_name():
    import random
    import string
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(20))

@receiver(post_save, sender=StudentImages, dispatch_uid="create_student_folder_handler")
def create_student_folder_handler(sender, instance, **kwargs):
    if instance.images: 
        # tạo một thư mục mới cho sinh viên
        student_folder = os.path.join(settings.MEDIA_ROOT, str(instance.student_code_id))
        os.makedirs(student_folder, exist_ok=True) # exist_ok=True: nếu thư mục đã tồn tại thì không tạo nữa

        old_path = os.path.normpath(instance.images.path) # đường dẫn ảnh cũ
        new_path = os.path.normpath(os.path.join(student_folder, os.path.basename(old_path))) # đường dẫn ảnh mới
        
        if old_path == new_path: # nếu ảnh cũ và ảnh mới giống nhau
            return
        # kiểm tra xem ảnh mới đã tồn tại chưa
        try:
            # nếu ảnh đã tồn tại thì sẽ xóa ảnh cũ
            if os.path.exists(new_path):
                os.remove(new_path)
        except StudentImages.DoesNotExist:
            pass

        # di chuyển ảnh mới
        if os.path.exists(new_path) and os.path.isfile(new_path): # nếu ảnh mới đã tồn tại
            os.replace(old_path, new_path) # thay thế ảnh cũ bằng ảnh mới
        else:
            if os.path.exists(old_path):
                os.rename(old_path, new_path) # di chuyển ảnh cũ đến ảnh mới
        
        #Đổi tên ảnh
        nameRandom = random_name()
        if os.path.exists(new_path):
            os.rename(new_path, os.path.join(student_folder, nameRandom + ".jpg"))
            new_path = os.path.join(student_folder, nameRandom + ".jpg")
        
        instance.images.name = os.path.relpath(new_path, settings.MEDIA_ROOT)
        instance.save()
        
@receiver(post_save, sender=Student)
def create_student_folder2(sender, instance, **kwargs):
        # Check if avatar is present
    if instance.avatar:
        # Tạo một thư mục mới cho sinh viên
        student_folder = os.path.join(settings.MEDIA_ROOT, str(instance.auto_id))
        os.makedirs(student_folder, exist_ok=True) # exist_ok=True: nếu thư mục đã tồn tại thì không tạo nữa

        # move image to the new folder if it has not already been moved
        if not instance.avatar.name.startswith(str(instance.auto_id)):
            old_path = instance.avatar.path
            new_path = os.path.join(student_folder, os.path.basename(old_path))
            if(os.path.exists(new_path)):
                os.remove(new_path)
            os.rename(old_path, new_path)
            
            #Đổi tên ảnh
            nameRandom = random_name()
            if os.path.exists(new_path):
                os.rename(new_path, os.path.join(student_folder, nameRandom + ".jpg"))
                new_path = os.path.join(student_folder, nameRandom + ".jpg")
            instance.avatar.name = os.path.relpath(new_path, settings.MEDIA_ROOT)
            instance.save()

@receiver(pre_delete, sender=StudentImages)
def delete_old_image(sender, instance, **kwargs):
    try:
        old_instance = StudentImages.objects.get(auto_id=instance.auto_id)
        if old_instance.images  and os.path.exists(old_instance.images.path):
            os.remove(old_instance.images.path)
    except StudentImages.DoesNotExist:
        pass

@receiver(pre_save, sender=Student)
def delete_old_image2(sender, instance, **kwargs):
    try:
        old_instance = Student.objects.get(student_code=instance.student_code)
        if old_instance.avatar and old_instance.avatar != instance.avatar and os.path.exists(old_instance.avatar.path):
            os.remove(old_instance.avatar.path)
    except Student.DoesNotExist:
        pass
    
@receiver(post_delete, sender=Student)
def delete_student(sender, instance, **kwargs):
    try:
        # Xóa thư mục của sinh viên
        student_folder = os.path.join(settings.MEDIA_ROOT, str(instance.auto_id))
        if os.path.exists(student_folder):
            shutil.rmtree(student_folder)
    except Student.DoesNotExist:
        pass
