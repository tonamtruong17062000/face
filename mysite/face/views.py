from datetime import date,datetime, timedelta
import os
import shutil
from .models import *
from app.models import *
from django.contrib.auth.models import User
from .common.common import capture_frames, recognize_face
from .common.train import train
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect
from django.core.paginator import Paginator
from django.http import HttpResponse, JsonResponse, HttpResponseServerError
import openpyxl
from django.db.models import Count
from django.core.files.storage import FileSystemStorage
from django.contrib.auth.hashers import make_password
from django.core.mail import send_mail
from django.db import transaction
from django.conf import settings
from django.utils.crypto import get_random_string
from django.utils import timezone
from django.core.files import File


# @login_required(login_url='/admin/login')
@user_passes_test(lambda u: u.is_staff, login_url='/admin/login') # Kiểm tra user có phải là staff hay không để truy cập vào trang này, nếu không phải thì chuyển hướng về trang login
def home(request):
    
    if request.method == 'POST':
        train()
        return HttpResponse('Train success')
    
    # Lấy số lượng sinh viên trong hệ thống
    totalStudent = Student.objects.all().count()
    
    # Lấy số lượng lớp học trong hệ thống
    totalClass = SubjectClass.objects.filter(created_by=request.user.username).count()
    
    # Lấy số lượng tỷ lệ vắng mặt của sinh viên
    percentAbsent = 0
    
    # Lấy số lượng sinh viên thiếu ảnh (số lượng nhỏ hon 60 ảnh)
    students = Student.objects.all() # Lấy danh sách sinh viên
    
    #Lấy ra những sinh viên chưa có ảnh hoặc ảnh ít hơn 60 ảnh
    student_mis_img = students.annotate(num_images=Count('studentimages')).filter(num_images__lt=60)
    totalStudentNoImage = student_mis_img.count()
    
    #Lấy ra tổng số sinh viên vắng mặt trong ngày có người tạo là user hiện tại
    totalAbsent = Attendance.objects.filter(created_by=request.user.username, is_present='0').count()
    
    #Lấy ra tổng số sinh viên có mặt trong ngày có người tạo là user hiện tại
    totalPresent = Attendance.objects.filter(created_by=request.user.username).count()
    
    if totalPresent > 0:
        percentAbsent = round((totalAbsent/totalPresent)*100,2)
    
    us = None

    if request.user.is_staff and not request.user.is_superuser:
        us = Teacher.objects.get(username=request.user.username)
    if request.user.is_superuser:
        us = request.user
    
    context = {"user": us,
               "totalStudent": totalStudent,
               "totalClass": totalClass,
               "percentAbsent": percentAbsent,
               "totalStudentNoImage": totalStudentNoImage,
               "student_mis_img": student_mis_img
               }
    print('------------',request.user.has_perm('add_teacher'))
    return render(request, 'pages/index.html',context)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login') 
def students(request):
    students = Student.objects.all().order_by('-created_at')
    page = None
    if request.method == 'POST':
        student_code = request.POST.get('student_code','')
        full_name = request.POST.get('full_name','')
        class_name = request.POST.get('class_name','')
        phone = request.POST.get('phone','')
    
        # Lấy danh sách sinh viên order by created_at desc

        if student_code != '':
            students = students.filter(student_code__icontains=student_code)
        if full_name != '':
            students = students.filter(full_name__icontains=full_name)
        if class_name != '':
            students = students.filter(class_name__icontains=class_name)
        if phone != '':
            students = students.filter(phone__icontains=phone)
        page = request.POST.get('page')
    
    paginator = Paginator(students, 10) # Mỗi trang chứa 1 sinh viên
    if page is not None:
        students = paginator.get_page(page)
    else:
        students = paginator.get_page(1)
    context = {"user": request.user, "students": students}
    return render(request, 'pages/student.html',context)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login') 
def my_logout(request):
    logout(request)
    return redirect('login')

@csrf_protect # Bảo vệ form. Điều này sẽ tạo ra một mã CSRF và đặt nó vào cookie, và sau đó kiểm tra mã này khi bạn gửi biểu mẫu.
def my_login(request):
    if request.method == 'POST':
        username = request.POST['email']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None and user.is_active:
            if user.is_staff:
                login(request, user)
                return redirect('home')
            else:
                messages.error(request, 'Tài khoản không có quyền truy cập chức năng này.')
        else:
            messages.error(request, 'Tài khoản hoặc mật khẩu không chính xác.')
    return render(request, 'pages/login.html')

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def student_detail(request, student_code):
    student = Student.objects.get(student_code=student_code)
    studentImages = StudentImages.objects.filter(student_code=student.auto_id)
    context = {'student': student, 'studentImages': studentImages}
    return render(request, 'pages/student_detail.html', context)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def student_update(request, student_code):
    student = Student.objects.get(student_code=student_code)
    if request.method == 'POST':
        student.student_code = request.POST['student_code']
        student.full_name = request.POST['full_name']
        student.class_name = request.POST['class_name']
        student.birth_date = request.POST['birth_date']
        student.phone = request.POST['phone']
        student.email = request.POST['email']
        student.address = request.POST['address']
        if 'avatar' in request.FILES:
            avatar = request.FILES['avatar']
            student.avatar = avatar
        if len(student.student_code) > 10:
            context = {'errorStudentCode': 'Mã sinh viên không được quá 10 ký tự.', 'student': student}
            return render(request, 'pages/student_update.html',context)
        
        if Student.objects.filter(student_code=student.student_code).exclude(student_code=student_code).exists():
            context = {'errorStudentCode': 'Mã sinh viên đã tồn tại.', 'student': student}
            return render(request, 'pages/student_update.html',context)
        
        if student_code != student.student_code and Student.objects.filter(student_code=student.student_code).exists() == False:
            # Cập nhật tài khoản sinh viên
            student.username = student.student_code
        student.save()
        return redirect('student_detail', student_code=student.student_code)
    context = {'student': student}
    return render(request, 'pages/student_update.html',context)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def student_delete(request, student_code):
    student = Student.objects.get(student_code=student_code)
    if student is not None and student.created_by != request.user.username:
            if request.user.is_superuser == False:
                messages.error(request, 'Bạn không có quyền xóa sinh viên này, chỉ quản trị viên hoặc giảng viên tạo mới có quyền xóa.')
                return redirect('students')
    student.delete()
    # Xóa tài khoản sinh viên
    user = User.objects.filter(username=student_code).first()
    if user is not None:
        user.delete()
    return redirect('students')

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def student_create(request):
    if request.method == 'POST':
        student = Student()
        student.student_code = request.POST['student_code']
        student.full_name = request.POST['full_name']
        student.class_name = request.POST['class_name']
        student.birth_date = request.POST['birth_date']
        student.phone = request.POST['phone']
        student.email = request.POST['email']
        student.address = request.POST['address']
        birth_day = request.POST['birth_date']
        student.created_by = request.user.username
        if 'avatar' in request.FILES:
            avatar = request.FILES['avatar']
            student.avatar = avatar
        
        if len(student.student_code) > 10:
            context = {'errorStudentCode': 'Mã sinh viên không được quá 10 ký tự.', 'student': student}
            return render(request, 'pages/student_update.html', context)
            
        if Student.objects.filter(student_code=student.student_code).exists():
            context = {'errorStudentCode': 'Mã sinh viên đã tồn tại.', 'student': student}
            return render(request, 'pages/student_update.html', context)
        if  student.student_code != '' and student.full_name != '' and student.class_name != '' and student.birth_date != '':
            password = datetime.strptime(birth_day, '%Y-%m-%d').date().strftime('%d/%m/%Y')
            student.set_password(password)
            student.username = student.student_code
            student.is_active = True
            student.save()
        else:
            context = {'errorStudentCode': 'Vui lòng nhập đầy đủ thông tin.', 'student': student}
            return render(request, 'pages/student_update.html', context)
        return redirect('student_detail', student_code=student.student_code)
    return render(request, 'pages/student_update.html')

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def student_multiple_create(request):
    if request.method == 'POST':
        errorNumber = 0
        file = request.FILES['excel_file']
        # Mở workbook
        workbook = openpyxl.load_workbook(file)
        # Lấy worksheet cần đọc dữ liệu
        worksheet = workbook.active
        # Khởi tạo danh sách students
        students = []
        # Duyệt từng row trong worksheet, bắt đầu từ row thứ 5
        index = 4
        for row in worksheet.iter_rows(min_row=5, values_only=True):
            index += 1
            errorIn = 0
            student = Student()
            
            # Kiểm tra birth_date có đúng định dạng không
            if not isinstance(row[3], date):
                errorIn += 1
                try:
                    worksheet.cell(row=4, column=8, value='Error')
                    cell = worksheet.cell(row=index, column=8)
                    value1 = cell.value
                    if value1 is None:
                        cell.value = "Ngày sinh không hợp lệ : " + str(row[3]) + "\t"
                    else:    
                        cell.value = str(value1) + "Ngày sinh không hợp lệ : " + str(row[3]) + "\t"
                except Exception as e:  
                    print(e)
            # Kiểm tra mã sinh viên đang đọc đã tồn tại trong database chưa
            if Student.objects.filter(student_code=row[0]).exists():
                # Nếu đã tồn tại thì bỏ qua
                errorIn += 1
                try:
                    #ghi log lỗi vào file excel
                    worksheet.cell(row=4, column=8, value='Error')
                    cell = worksheet.cell(row=index, column=8)
                    value2 = cell.value
                    if value2 is None:
                        cell.value = "Mã sinh viên đã tồn tại: " + str(row[0]) + "\t"
                    else:
                        cell.value = str(value2) + "Mã sinh viên đã tồn tại: " + str(row[0]) + "\t"
                except Exception as e:  
                    print(e)

            if errorIn == 0:
                # Tạo instance của Student và đọc giá trị từng cột trong row
                student = Student(
                        student_code=row[0],
                        full_name=row[1],
                        class_name=row[2],
                        birth_date=row[3],
                        phone=row[4],
                        email=row[5],
                        address=row[6],
                        created_by=request.user.username
                    )
                students.append(student)
            else:
                errorNumber += 1
                continue
            
        # Lưu danh sách students vào database
        if errorNumber == 0:
            with transaction.atomic():
                for student in students:
                    password = student.birth_date.strftime('%d/%m/%Y')
                    student.set_password(password)
                    student.username = student.student_code
                    student.is_active = True
                    student.save()
                messages.success(request, 'Thêm mới thành công!')
                return redirect('students')
            
        else:
            workbook.save(file)
            # Khởi tạo file response
            response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            
            # Tạo tên file
            response['Content-Disposition'] = 'attachment; filename=student-error.xlsx'
            
            # Lưu file excel vào response
            workbook.save(response)
            return response

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def studentImages(request, student_code):
    errors = None
    studentID = Student.objects.get(student_code=student_code).auto_id
    if request.method == 'POST':
        #Kiem tra xem sinh vien da du 100 anh tren he thong chua
        totalImageCurrent = StudentImages.objects.filter(student_code=studentID).count()
        if totalImageCurrent < 100:
            images = request.FILES.getlist('images') # getlist() returns a list of files
            # Lay ra so luong anh da duoc upload
            totalImages = images.__len__()
            
            if totalImages + totalImageCurrent <= 100:
                student = Student.objects.get(student_code=student_code)
                for image in images:
                    StudentImages.objects.create(student_code=student, images=image, created_by=request.user.username)
            else:
                errors = []
                errors.append('Mỗi sinh viên chỉ được upload tối đa 100 ảnh trên hệ thống!')
                errors.append('Bạn đã upload ' + str(totalImageCurrent) + ' ảnh, chỉ được thêm ' + str(100 - totalImageCurrent) + ' ảnh nữa!')
                errors.append('Vui lòng xóa bớt ảnh cũ trước khi upload ảnh mới hoặc giảm số lượng ảnh upload!')
        else:
                errors = []
                errors.append('Sinh viên này đã upload đủ 100 ảnh trên hệ thống, không thể upload thêm ảnh nữa!')
            
    studentImages = StudentImages.objects.filter(student_code=studentID).order_by('-created_at')
    context = {'studentImages': studentImages,"errors":errors, 'student_code': student_code}
    return render(request, 'pages/student_images.html',context)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def video(request, student_code):
    if request.method == 'POST':
        student = Student.objects.get(student_code=student_code)
        video = request.FILES['video']
        if video is not None:
            #check file video is video
            if video.name.endswith('.mp4') or video.name.endswith('.avi') or video.name.endswith('.mov'):
                #check file video is not empty
                if video.size > 0:
                    #check file video is not larger than 50MB
                    if video.size < 52428800:
                        print(video.size)
                    else:
                        print('Video quá lớn, vui lòng chọn video nhỏ hơn 50MB!')
                else:
                    messages.error(request, 'Video không được để trống!')
            
            # Lưu video lại
            fs = FileSystemStorage(location='media/face/')
            
            # Lưu video vào thư mục media/videos
            vs = fs.save('videos/' + video.name, video)
            
            # Lấy đường dẫn tương đối của video
            pathVideo = 'media/face/videos/' + video.name

            pathImgs = capture_frames(pathVideo, 3)
            for path in pathImgs:
                with open(path, 'rb') as img_file:
                    img_field = File(img_file)
                    StudentImages.objects.create(student_code=student, images=img_field)
                    
            # Xóa video trong thư mục media/videos
            fs.delete('videos/' + video.name)
            
            # Xóa folder temp
            shutil.rmtree('media/face/temp')
            #Tạo folder temp mới
            os.mkdir('media/face/temp')
    return redirect('student_images', student_code=student_code)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def studentImages_delete(request, student_code):
    if request.method == 'POST':
        auto_id = request.POST['auto_id']
        StudentImages.objects.filter(auto_id=auto_id).delete()
    return redirect('student_images', student_code=student_code)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def face(request):
    if request.method == 'POST':
        imgFile = request.FILES.get('image')
        if imgFile is not None:
            fs = FileSystemStorage(location="media/face/")
            filename = fs.save('imgs_request/' + imgFile.name, imgFile)
            #get path of image file
            path = fs.path(filename)
            print(path)
            student = recognize_face(path)
            print(student)
            return redirect('home')
        else:
            print('Khong co anh')
    return redirect('home')

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def faceApi(request):
    if request.method == 'POST':
        imgFile = request.FILES.get('image')
        if imgFile is not None:
            fs = FileSystemStorage(location="media/face/")
            filename = fs.save('imgs_request/' + imgFile.name, imgFile)
            #get path of image file
            path = fs.path(filename)
            student = recognize_face(path)
            return JsonResponse(student.serializable())
        else:
            return HttpResponse('Khong co anh')
    return HttpResponse('Không hỗ trợ phương thức này!')

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def classes(request):
    class_code = request.GET.get('class_code','')
    class_name = request.GET.get('class_name','')
    hoc_ky = request.GET.get('hoc_ky','')
    nam_hoc = request.GET.get('nam_hoc','')

     # Lấy danh sách sinh viên order by created_at desc
    subjectClass = SubjectClass.objects.all().order_by('-created_at')
    
    if class_code != '':
        subjectClass = subjectClass.filter(class_code__icontains=class_code)
    if class_name != '':
        subjectClass = subjectClass.filter(class_name__icontains=class_name)
    if hoc_ky != '':
        subjectClass = subjectClass.filter(hoc_ky__icontains=hoc_ky)
    if nam_hoc != '':
        subjectClass = subjectClass.filter(nam_hoc__icontains=nam_hoc)
        
    subjectClass = subjectClass.filter(created_by=request.user.username)
    
    paginator = Paginator(subjectClass, 10) # Mỗi trang chứa 10 item
    page = request.GET.get('page')
    subjectClass = paginator.get_page(page)
    context = {"user": request.user, "subjectClass": subjectClass}
    return render(request, 'pages/lophoc/class.html', context)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def classes_create(request):
    if request.method == 'POST':
        class_code = request.POST['class_code']
        ma_hoc_phan = request.POST['ma_hoc_phan']
        class_name = request.POST['class_name']
        hoc_ky = request.POST['hoc_ky']
        nam_hoc = request.POST['nam_hoc']
        subjectClass = SubjectClass(class_code=class_code, ma_hoc_phan=ma_hoc_phan, class_name=class_name, hoc_ky=hoc_ky, nam_hoc=nam_hoc)
        if len(class_code) > 30:
            context = {"user": request.user, "error": "Mã lớp học không được quá 30 ký tự!","subjectClass": subjectClass}
        
        if SubjectClass.objects.filter(class_code=class_code).exists():
            context = {"user": request.user, "error": "Mã lớp học đã tồn tại!","subjectClass": subjectClass}
            return render(request, 'pages/lophoc/class_update.html', context)
        else:
            SubjectClass.objects.create(class_code=class_code, ma_hoc_phan=ma_hoc_phan, class_name=class_name, hoc_ky=hoc_ky, nam_hoc=nam_hoc, created_by=request.user.username)
            return redirect('classes')
    return render(request, 'pages/lophoc/class_update.html')

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def classes_update(request, class_code):
    if request.method == 'POST':
        class_code_request = request.POST['class_code']
        ma_hoc_phan = request.POST['ma_hoc_phan']
        class_name = request.POST['class_name']
        hoc_ky = request.POST['hoc_ky']
        nam_hoc = request.POST['nam_hoc']
        subjectClass = SubjectClass(class_code=class_code_request, ma_hoc_phan=ma_hoc_phan, class_name=class_name, hoc_ky=hoc_ky, nam_hoc=nam_hoc)
        if len(class_code_request) > 30:
                context = {"user": request.user, "error": "Mã lớp học không được quá 30 ký tự!","subjectClass": subjectClass}
                return render(request, 'pages/lophoc/class_update.html', context)
        if class_code_request != class_code:
            if SubjectClass.objects.filter(class_code=class_code_request).exists():
                context = {"user": request.user, "error": "Mã lớp học đã tồn tại!","subjectClass": subjectClass}
                return render(request, 'pages/lophoc/class_update.html', context)
            else:
                SubjectClass.objects.filter(class_code=class_code).update(class_code=class_code_request, ma_hoc_phan=ma_hoc_phan, class_name=class_name, hoc_ky=hoc_ky, nam_hoc=nam_hoc, update_by=request.user, updated_at=datetime.now())
                return redirect('classes')
        else:
            SubjectClass.objects.filter(class_code=class_code).update(class_name=class_name, ma_hoc_phan=ma_hoc_phan, hoc_ky=hoc_ky, nam_hoc=nam_hoc, update_by=request.user, updated_at=datetime.now())
            return redirect('classes')
    subjectClass = SubjectClass.objects.get(class_code=class_code)
    context = {"user": request.user, "subjectClass": subjectClass}
    return render(request, 'pages/lophoc/class_update.html', context)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def classes_delete(request):
    result = {"status": 500}
    if request.method == 'POST':
        class_code = request.POST['class_code']
        try:
            subjectClass = SubjectClass.objects.get(class_code=class_code, created_by=request.user.username)
            subjectClass.delete()
            result = {"status": 200}
        except SubjectClass.DoesNotExist:
            result = {"status": 404, "message": "Lớp không tồn tại!"}
        except Exception as e:
            result = {"status": 500, "message": str(e)}
            return HttpResponseServerError(JsonResponse(result))
    return JsonResponse(result)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def classes_details(request, class_code):
    subjectClass = SubjectClass.objects.get(class_code=class_code)
    context = {"user": request.user, "subjectClass": subjectClass}
    return render(request, 'pages/lophoc/class_details.html',context)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def class_attendance(request, class_code):

    # Lấy thông tin lớp học
    subjectClass = SubjectClass.objects.get(class_code=class_code)

    #Lấy danh sách sinh viên trong lớp
    studentInSubjectClass = StudentInSubjectClass.objects.filter(class_code=subjectClass)
    
    #Lấy ra các lần điểm danh
    attendances = Attendance.objects.filter(subject_class=subjectClass).order_by('date_time')
    
    attendanceTimeAndImg = []
    attendanceTimeAndImgTemp = []
    
    #Lấy ra danh sách thời gian điểm danh group by theo date_time
    for item in attendances:
        if item.date_time not in attendanceTimeAndImgTemp:
            attendanceTimeAndImgTemp.append(item.date_time)
            attendanceTimeAndImg.append(AttendanceTimeAndImg(item.date_time, item.imagePath, item.created_by))
            
    #Lấy ra note các lần học của lớp học
    learns = Learning.objects.filter(subject_class=subjectClass)
    pathFileLearnings = PathFileLearning.objects.filter(learning__in=learns)
    
    context = {"attendances": attendances,
               "subjectClass": subjectClass,
               "studentInSubjectClass": studentInSubjectClass,
               "attendanceTimeAndImgs": attendanceTimeAndImg,
                "learns": learns,
                "pathFileLearnings": pathFileLearnings
               }
    
    return render(request, 'pages/lophoc/hoc_tap_va_diem_danh.html',context)

def forgotPassword(request):
    context ={}
    if request.method == 'POST':
        email = request.POST['email']
        if User.objects.filter(email=email).exists():
            user = User.objects.filter(email=email).first()
             #Tạo chuỗi token ngẫu nhiên
            token = get_random_string(length=30)
            now = datetime.now()
            expired_at = now + timedelta(minutes=5)
            forgotPassword = ForgotPassword(user=user, token=token, expired=expired_at, isUsed= '0')
            forgotPassword.save()
            
            
            #Gửi mail
            subject = 'Quên mật khẩu'
            message = 'Chào bạn, \n\nBạn vừa yêu cầu đổi mật khẩu tại hệ thống điểm danh trực tuyến.\
                \n\nNhấn vào đường link sau để đổi mật khẩu: \n\nhttp://' + request.get_host() + '/doi-mat-khau/' + \
                token + '\n\nNếu bạn không yêu cầu đổi mật khẩu, vui lòng bỏ qua email này.\
                    \n\nĐường link có hiệu lực trong 5 phút.\
                    \n\nTrân trọng, \n\nHệ thống điểm danh trực tuyến.'
            from_email = settings.EMAIL_HOST_USER
            to_list = [email]
            status = send_mail(subject, message, from_email, to_list, fail_silently=True)
            if status == 1:
                context = {"status": 200,'message': 'Email đã được gửi đến bạn. Vui lòng kiểm tra email để đổi mật khẩu!'}
            else:
                context = {"status": 500,'message': 'Gửi email thất bại!'}
    return render(request, 'pages/quen_mat_khau.html',context)

def datLaiMatKhau(request, token):
    now = datetime.now()
    if request.method == 'POST':
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        if password == confirm_password:
            forgotPassword = ForgotPassword.objects.filter(token=token, isUsed='0').first()
            if forgotPassword is not None:
                now = datetime.now()
                expired = forgotPassword.expired
                expired = expired.replace(tzinfo=None)
                if expired > now:
                    user = forgotPassword.user
                    user.set_password(password)
                    user.save()
                    forgotPassword.isUsed = '1'
                    forgotPassword.save()
                    context = {"status": 200}
                    return render(request, 'pages/quen_mat_khau_2.html', context)
            else:
                return render(request, '404.html')
        else:
            error1 = 'Mật khẩu nhập lại không trùng khớp!'
            context = {"error1": error1}
            return render(request, 'pages/quen_mat_khau_2.html', context)
    forgotPassword = ForgotPassword.objects.filter(token=token, isUsed='0').first()
    if forgotPassword is not None:
        expired = forgotPassword.expired
        expired = expired.replace(tzinfo=None)
        if expired >= now:
            return render(request, 'pages/quen_mat_khau_2.html', {'user': forgotPassword.user})
    return render(request, '404.html')

@login_required(login_url='/login')
def change_password(request):
    if request.method == 'POST':
        old_password = request.POST['old_password']
        new_password = request.POST['new_password']
        confirm_password = request.POST['confirm_password']
        if request.user.check_password(old_password):
            if new_password == confirm_password:
                request.user.set_password(new_password)
                request.user.save()
                # Đăng xuất
                logout(request)

                context = {"user": request.user, "status": 200}
                print('Đổi mật khẩu thành công!')
                return render(request, 'pages/doi_mat_khau.html', context)
            else:
                context = {"user": request.user, "error1": "Mật khẩu nhập lại không trùng khớp!"}
                return render(request, 'pages/doi_mat_khau.html', context)
        else:
            context = {"user": request.user, "error": "Mật khẩu không đúng!"}
            return render(request, 'pages/doi_mat_khau.html', context)
    return render(request, 'pages/doi_mat_khau.html')
# ---------------------------------TEACHER BEGIN---------------------------------
@user_passes_test(lambda u: u.is_superuser, login_url='/admin/login')
def teachers(request):
    teachers = Teacher.objects.all().order_by('-created_at')
    page = None
    if request.method == 'POST':
        teacher_code = request.POST.get('teacher_code','')
        full_name = request.POST.get('full_name','')
        faculty = request.POST.get('faculty','')
        department = request.POST.get('department','')
    
        # Lấy danh sách sinh viên order by created_at desc

        if teacher_code != '':
            teachers = teachers.filter(teacher_code__icontains=teacher_code)
        if full_name != '':
            teachers = teachers.filter(full_name__icontains=full_name)
        if faculty != '':
            teachers = teachers.filter(faculty__icontains=faculty)
        if department != '':
            teachers = teachers.filter(department__icontains=department)
        page = request.POST.get('page')
    
    paginator = Paginator(teachers, 10) # Mỗi trang chứa 10 teacher
    if page is not None:
        teachers = paginator.get_page(page)
    else:
        teachers = paginator.get_page(1)
    context = {"user": request.user, "teachers": teachers}
    return render(request, 'pages/giang_vien.html',context)

@user_passes_test(lambda u: u.is_superuser, login_url='/admin/login')
def teacher_update(request, teacher_code):
    teacher = Teacher.objects.get(teacher_code=teacher_code)
    if request.method == 'POST':
        teacher.teacher_code = request.POST['teacher_code']
        teacher.full_name = request.POST['full_name']
        teacher.birth_date = request.POST['birth_date']
        teacher.phone = request.POST['phone']
        teacher.address = request.POST['address']
        teacher.email = request.POST['email']
        teacher.faculty = request.POST['faculty']
        teacher.department = request.POST['department']
        if 'avatar' in request.FILES:
            avatar = request.FILES['avatar']
            # Xóa ảnh cũ của teacher
            avatar_old = teacher.avatar
            if avatar_old != '':
                # Lấy tên ảnh trong path avatar_old
                avatar_old = avatar_old.split('/')[-1]
                # Xóa ảnh cũ
                os.remove(os.path.join(settings.MEDIA_TEACHER, avatar_old))
            # Lưu ảnh mới
            fs = FileSystemStorage(location=settings.MEDIA_TEACHER)
            filename = fs.save(avatar.name, avatar)
            teacher.avatar = '/teacher/' + filename
            
        if len(teacher.teacher_code) > 10:
            context = {'errorStudentCode': 'Mã giảng viên không được quá 10 ký tự.', 'teacher': teacher}
            return render(request, 'pages/giang_vien_update.html',context)
        
        if Teacher.objects.filter(teacher_code=teacher.teacher_code).exclude(teacher_code=teacher_code).exists():
            context = {'errorStudentCode': 'Mã giảng viên đã tồn tại.', 'teacher': teacher}
            return render(request, 'pages/giang_vien_update.html',context)
        
        if teacher_code != teacher.teacher_code and Teacher.objects.filter(teacher_code=teacher.teacher_code).exists() == False:
            # Cập nhật tài khoản sinh viên
            teacher.username = teacher.teacher_code
        teacher.save()
        return redirect('teachers')
    context = {'teacher': teacher}
    return render(request, 'pages/giang_vien_update.html',context)

@user_passes_test(lambda u: u.is_superuser, login_url='/admin/login')
def teacher_create(request):
    teacher = Teacher()
    if request.method == 'POST':
        teacher.teacher_code = request.POST['teacher_code']
        teacher.full_name = request.POST['full_name']
        teacher.birth_date = request.POST['birth_date']
        teacher.phone = request.POST['phone']
        teacher.address = request.POST['address']
        teacher.faculty = request.POST['faculty']
        teacher.email = request.POST['email']
        teacher.department = request.POST['department']
        birth_day = request.POST['birth_date']
        teacher.created_by = request.user.username
        if 'avatar' in request.FILES:
            avatar = request.FILES['avatar']
            # Lưu avatar vào thư mục media/face/imgs_teacher
            fs = FileSystemStorage(location='media/face/imgs_teacher')
            filename = fs.save(avatar.name, avatar)
            teacher.avatar = '/teacher/' + filename
        
        if len(teacher.teacher_code) > 10:
            context = {'errorStudentCode': 'Mã giảng viên không được quá 10 ký tự.', 'teacher': teacher}
            return render(request, 'pages/giang_vien_update.html', context)
            
        if Teacher.objects.filter(teacher_code=teacher.teacher_code).exists():
            context = {'errorStudentCode': 'Mã giảng viên đã tồn tại.', 'teacher': teacher}
            return render(request, 'pages/giang_vien_update.html', context)
        if  teacher.teacher_code != '' and teacher.full_name != '' and teacher.department != '' and teacher.birth_date != '':
            password = datetime.strptime(birth_day, '%Y-%m-%d').date().strftime('%d/%m/%Y')
            teacher.set_password(password)
            teacher.username = teacher.teacher_code
            teacher.is_active = True
            teacher.is_staff = True
            teacher.save()
        else:
            context = {'errorStudentCode': 'Vui lòng nhập đầy đủ thông tin.', 'teacher': teacher}
            return render(request, 'pages/giang_vien_update.html', context)
        return redirect('teachers')
    context = {'teacher': teacher}
    return render(request, 'pages/giang_vien_update.html', context)

@user_passes_test(lambda u: u.is_superuser, login_url='/admin/login')
def teacher_delete(request, teacher_code):
    teacher = Teacher.objects.get(teacher_code=teacher_code)
    if teacher is not None and request.user.is_superuser:
        teacher.delete()
    # Xóa tài khoản sinh viên
    user = User.objects.filter(username=teacher_code).first()
    if user is not None:
        user.delete()
    return redirect('teachers')

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def teacher_profile(request, teacher_code):
    teacher = Teacher.objects.get(teacher_code=teacher_code)
    context = {'teacher': teacher}
    return render(request, 'pages/giang_vien_profile.html', context)
#--------------------------------------END TEACHER--------------------------------------

    