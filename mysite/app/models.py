import json
from django.db import models
from django.contrib.auth.models import User
from enum import Enum
# Create your models here.

class ResponseStatus(Enum):
    SUCCESS = 200
    ERROR = 400
    NOT_FOUND = 404

class Response:
    def __init__(self, code, desc, result):
        self.code = code
        self.desc = desc
        self.result = result

    def to_dict(self):
        return {
            'code': self.code,
            'desc': self.desc,
            'result': self.result,
        }

    def to_json(self):
        return json.dumps(self.to_dict())

class ForgotPassword(models.Model):
    id = models.AutoField(primary_key=True)
    token = models.CharField(max_length=30, blank=False, unique=True, default=None)
    expired = models.DateTimeField(null=True)
    isUsed = models.CharField(max_length=1, blank=False, default='0')
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    
class AttendanceTimeAndImg:
    date = models.DateTimeField(null=True)
    path = models.CharField(max_length=100, blank=False, null=True)
    created_by = models.CharField(max_length=100, blank=False, null=True)
    
    def __str__(self):
        return self.path
    
    def __init__(self, date, path, created_by):
        self.date = date
        self.path = path
        self.created_by = created_by
    
    def to_dict(self):
        return {
            'date': self.date.strftime("%d-%m-%Y %H:%M:%S"),
            'path': self.path,
            'created_by': self.created_by,
        }
    
    def to_json(self):
        return json.dumps(self.to_dict())
   