from django.urls import path
from app import views as app_views


urlpatterns = [
    path('excel', app_views.create_excel_template, name='create_excel_template'),
    path('export', app_views.export, name='export'),
    path('student', app_views.search_student, name='search_student_api'),
    path('studentInClass', app_views.studentInClass, name='student_in_class_api'),
    path('addStudentToClass', app_views.add_student_to_class, name='add_student_to_class_api'),
    path('deleteStudentFromClass', app_views.delete_student_in_class, name='delete_student_from_class_api'),
    path('attendances', app_views.attendances, name='attendances_api'),
    path('save-attendance', app_views.saveAttendance, name='save_attendance_api'),
    path('update-attendance', app_views.update_attendance, name='update_attendance_api'),
    path('delete-attendance', app_views.delete_attendance, name='delete_attendance_api'),
    path('recognize', app_views.recognize_Face, name='recognize_api'),
    path('statistical', app_views.statistical, name='statistical_api'),
    path('send-mail', app_views.send_email_api, name='send_mail_api'),
    path('import_students_to_subject/<str:class_code>', app_views.import_students_to_subjectclass, name='import_students_to_subject_api'),
    path('learn/<str:class_code>', app_views.learns_create, name='learn_create_api'),
    path('learn-delete/<str:auto_id>', app_views.learns_delete, name='learn_delete_api'),
    path('document-delete/<str:auto_id>', app_views.deleteDocument, name='document_delete_api'),
]
