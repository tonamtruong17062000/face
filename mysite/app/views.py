import traceback
from django.shortcuts import render
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import *
from django.conf import settings
import openpyxl
from openpyxl.utils import get_column_letter
from django.contrib.auth.decorators import login_required, user_passes_test
from face.models import *
import json, xlrd, xlwt
from django.core.files.storage import FileSystemStorage
from face.common.common import recognize_face
from .constanst import *
from datetime import datetime
from django.views.decorators.http import require_http_methods
import cv2
import numpy as np
from bs4 import BeautifulSoup
from django.core.mail import send_mail, EmailMultiAlternatives
from django.utils.html import strip_tags


#Nhận diện khuôn măt: 
@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def recognize_Face(request):
    if request.method == 'POST':
        imgFile = request.FILES.get('image')
        fs = FileSystemStorage(location="media/face/")
        filename = fs.save('imgs_request/a.jpg', imgFile)
        #get path of image file
        path = fs.path(filename)
        
        # Đọc ảnh từ path
        image = cv2.imread(path)
        
        # Chuyển ảnh về ảnh xám
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) #CHuyển ảnh về ảnh xám với kênh màu là 1
        
        # Tăng độ tương phản bằng histogram
        # gray = cv2.equalizeHist(gray)
        
        # Cân bằng sáng
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8)) # Tạo bộ cân bằng sáng
        gray = clahe.apply(gray) # Áp dụng bộ cân bằng sáng
                
        # Nạp bộ nhận diện khuôn mặt và mắt
        face_cascade = cv2.CascadeClassifier(str(settings.BASE_DIR) + "/static/data/haarcascades/haarcascade_frontalface_alt2.xml")
        eyes_cascade = cv2.CascadeClassifier(str(settings.BASE_DIR) + "/static/data/haarcascades/haarcascade_eye.xml")
        
        # Cắt ảnh chỉ lấy khuôn mặt
        faces = face_cascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5, minSize=(50, 50))
        
        # Nếu có khuôn mặt thì tiến hành nhận diện
        if len(faces) > 0:

            for (x, y, w, h) in faces:
                # Cắt ảnh khuôn mặt
                roi_gray = gray[y:y+h, x:x+w]
                
                # Nhận diện mắt trong khuôn mặt
                eyes = eyes_cascade.detectMultiScale(roi_gray)
                    
                # Xác tâm 2 mắt
                if len(eyes) == 2:
                    
                    #Vẽ hình chữ nhật quanh mắt
                    for (ex, ey, ew, eh) in eyes:
                        cv2.rectangle(roi_gray, (ex, ey), (ex+ew, ey+eh), (255, 0, 0), 2)
                    
                    # Tọa độ tâm mắt trái
                    x1 = eyes[0][0] + eyes[0][2]/2
                    y1 = eyes[0][1] + eyes[0][3]/2
                    
                    # Tọa độ tâm mắt phải
                    x2 = eyes[1][0] + eyes[1][2]/2
                    y2 = eyes[1][1] + eyes[1][3]/2
                    
                    #Vẽ đường nôi 2 mắt
                    cv2.line(roi_gray, (int(x1), int(y1)), (int(x2), int(y2)), (0, 255, 0), 2)
                                        
                    # Nếu tâm mắt trái nằm cao hơn tâm mắt phải thì xoay ảnh theo tâm mắt trái ngược chiều kim đồng hồ
                    if y1 > y2:
                        angle = np.arctan((y1-y2)/(x1-x2)) * 180 / np.pi # Góc xoay bằng arctan của hệ số góc * 180 / pi (đổi sang độ)
                    else:
                        angle = np.arctan((y2-y1)/(x2-x1)) * 180 / np.pi # Góc xoay bằng arctan của hệ số góc * 180 / pi (đổi sang độ)
                        
                    # Xoay ảnh
                    (h, w) = roi_gray.shape[:2] # Lấy chiều cao và chiều rộng của ảnh
                    center = (w / 2, h / 2) # Tọa độ tâm ảnh
                    M = cv2.getRotationMatrix2D(center, angle, 1.0) # Tạo ma trận xoay ảnh băng hàm getRotationMatrix2D với tham số tâm ảnh, góc xoay, tỉ lệ
                    roi_gray = cv2.warpAffine(roi_gray, M, (w, h)) # Xoay ảnh
                    
                    #Lật ảnh đối xứng dọc
                    roi_gray = cv2.flip(roi_gray, 1)
                    
                    cv2.imwrite(path, roi_gray)
        
                    response = Response(SUCCESS_CODE, SUCCESS_MESSAGE, None)
                    return JsonResponse(response.to_dict(), safe=False)
                    
                else:
                    print("Không nhận diện được mắt")
                
            # Lưu ảnh kết quả, ghi đè lên ảnh gốc
        cv2.imwrite(path, roi_gray)
        
    response = Response(SUCCESS_CODE, SUCCESS_MESSAGE, None)
    return JsonResponse(response.to_dict(), safe=False)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def create_excel_template(request):
    # Tạo workbook mới
    wb = openpyxl.Workbook()
    # Lấy sheet đầu tiên
    sheet = wb.active
    
    # Ghi chú
    sheet['A1'] = 'Học viện nông nghiệp Việt Nam'
    #merge cell
    sheet['A2'] = 'Mẫu nhập dữ liệu sinh viên'
    
    # Thêm các tiêu đề cột
    sheet['A4'] = 'Mã SV'
    sheet['B4'] = 'Họ tên'
    sheet['C4'] = 'Lớp'
    sheet['D4'] = 'Ngày sinh'
    sheet['E4'] = 'Số điện thoại'
    sheet['F4'] = 'Email'
    sheet['G4'] = 'Địa chỉ'
    
    # Thêm một số giá trị mặc định
    sheet['A5'] = '637960'
    sheet['B5'] = 'Nguyễn Văn A'
    sheet['C5'] = 'K63CNPMP'
    sheet['D5'].number_format = 'dd/mm/yyyy'
    sheet['D5'] = datetime(2000, 6, 17)
    sheet['E5'] = '0987656886'
    sheet['F5'] = 'example@example.com'
    sheet['G5'] = '17 Ngõ 239 Trâu Quỳ - Gia Lâm - Hà Nội'
    
    #border cho tất cả các cell từ A4 đến G5
    for row in sheet.iter_rows(min_row=4, max_row=5, min_col=1, max_col=7):
        row[0].border = row[1].border = row[2].border = row[3].border = row[4].border = row[5].border = row[6].border = openpyxl.styles.Border(left=openpyxl.styles.Side(border_style='thin', color='FF000000'), right=openpyxl.styles.Side(border_style='thin', color='FF000000'), top=openpyxl.styles.Side(border_style='thin', color='FF000000'), bottom=openpyxl.styles.Side(border_style='thin', color='FF000000'))
    #Tự động dãn cột theo nội dung
    sheet.column_dimensions['A'].auto_size = True
    sheet.column_dimensions['B'].auto_size = True
    sheet.column_dimensions['C'].auto_size = True
    sheet.column_dimensions['D'].auto_size = True
    sheet.column_dimensions['E'].auto_size = True
    sheet.column_dimensions['F'].auto_size = True
    sheet.column_dimensions['G'].auto_size = True
    
    # Khởi tạo HTTP response
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    
    # Đặt tên file
    response['Content-Disposition'] = 'attachment; filename="student_template.xlsx"'
    
    # Lưu workbook vào response
    wb.save(response)
    
    return response

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def import_students_to_subjectclass(request, class_code):
    if request.method == 'POST':
        file = request.FILES['file']
        subjectClass = SubjectClass.objects.get(auto_id=class_code)
        context = {"user": request.user, "subjectClass": subjectClass}
        errorCode = 0
        if not file.name.endswith('.xlsx') and not file.name.endswith('.xls'):
            context = {"user": request.user, "error": "Định dạng file không đúng!"}
        else:
            try:
                if file.name.endswith('.xlsx'):
                    workbook = openpyxl.load_workbook(file)
                    print('extention xlsx')
                    #Đọc file excel bắt đầu từ dòng 11 đến dòng cuối cùng có dữ liệu
                    sheet = workbook.active
                    
                    #Tạo cột Lỗi trong file excel
                    sheet.cell(row=10, column=10, value='Thông báo')
                    for row in sheet.iter_rows(min_row=11, max_col=2, max_row=sheet.max_row):
                        if row[0].value is None:
                            errorCode += 1
                            break
                        else:
                            #Lấy thông tin sinh viên
                            student_code = row[1].value

                            #Kiểm tra mã sinh viên đã tồn tại trong hệ thống chưa
                            if Student.objects.filter(student_code=student_code).exists():
                                student = Student.objects.get(student_code=student_code)
                                
                                #Kiểm tra sinh viên đã có trong lớp học chưa
                                if StudentInSubjectClass.objects.filter(student_code=student, class_code=subjectClass).exists():
                                    errorCode += 1
                                    
                                    #Ghi thông tin lỗi dòng hiện tại cột số 10
                                    sheet.cell(row=row[0].row, column=10, value='Sinh viên đã có trong lớp học!')
                                else:  
                                    #Tạo mới sinh viên trong lớp học
                                    studentInSubjectClass = StudentInSubjectClass(student_code=student, class_code=subjectClass, created_by=request.user.username)
                                    studentInSubjectClass.save()
                                    sheet.cell(row=row[0].row, column=10, value='Thêm sinh viên thành công!')
                                    # Kiểm tra lịch sử điểm danh của lớp
                                    attendance = Attendance.objects.filter(subject_class=subjectClass)
                                    
                                    #Lấy ra những lần điểm danh của lớp học
                                    date = attendance.values_list('date_time', flat=True)     
                                    
                                    #Lấy ra những lần điểm danh duy nhất
                                    date = list(set(date))
                                    for d in date:
                                        #Kiểm tra tại mỗi lần d sinh viên đã có dữ liệu điểm danh chưa
                                        attendance = Attendance.objects.filter(subject_class=subjectClass, date_time=d, student_code=student)
                                        if attendance.count() == 0:
                                            #Nếu chưa có thì thêm vào với giá trị mặc định là 0
                                            Attendance(subject_class=subjectClass, 
                                                    student_code=student, date_time=d, 
                                                    created_by = request.user.username, is_present = '0').save()
                            else:
                                errorCode += 1
                                sheet.cell(row=row[0].row, column=10, value='Mã sinh viên không tồn tại trong hệ thống!')
                    #Lưu file excel
                    workbook.save(file)

                    #Khởi tạo response
                    response = HttpResponse(content_type='application/vnd.ms-excel')
                    response['Content-Disposition'] = 'attachment; filename=error.xlsx'
                    workbook.save(response)
                    return response
                else:
                    wb = xlrd.open_workbook(file_contents=file.read())
                    
                    #Đọc file excel bắt đầu từ dòng 11 đến dòng cuối cùng có dữ liệu
                    sheet = wb.sheet_by_index(0)
                    
                    #Tạo cột Lỗi trong file excel
                    sheet.put_cell(9, 9, xlrd.XL_CELL_TEXT, 'Thông báo', None)
                    for row in range(10, sheet.nrows):
                        if sheet.cell_value(row, 0) == '':
                            errorCode += 1
                            break
                        else:
                            #Lấy thông tin sinh viên
                            student_code = sheet.cell_value(row, 1)
                            #Kiểm tra mã sinh viên đã tồn tại trong hệ thống chưa
                            if Student.objects.filter(student_code=student_code).exists():
                                student = Student.objects.get(student_code=student_code)
                                
                                #Kiểm tra sinh viên đã có trong lớp học chưa
                                if StudentInSubjectClass.objects.filter(student_code=student, class_code=subjectClass).exists():
                                    errorCode += 1
                                    
                                    #Ghi thông tin lỗi dòng hiện tại cột số 10
                                    sheet.put_cell(row, 9, xlrd.XL_CELL_TEXT, 'Sinh viên đã có trong lớp học!', None)
                                else:  
                                    #Tạo mới sinh viên trong lớp học
                                    studentInSubjectClass = StudentInSubjectClass(student_code=student, class_code=subjectClass, created_by=request.user.username)
                                    studentInSubjectClass.save()
                                    sheet.put_cell(row, 9, xlrd.XL_CELL_TEXT, 'Thêm sinh viên thành công!', None)
                            else:
                                errorCode += 1
                                sheet.put_cell(row, 9, xlrd.XL_CELL_TEXT, 'Mã sinh viên không tồn tại trong hệ thống!', None)
                    #Lưu file excel
                    
                    # Tạo một workbook mới để ghi dữ liệu
                    new_workbook = openpyxl.Workbook()
                    
                    # Tạo một sheet mới trong workbook mới
                    new_sheet = new_workbook.active
                    
                    # Đọc dữ liệu từ sheet sang new_sheet
                    for row in range(9, sheet.nrows):
                        if sheet.cell_value(row, 0) == '':
                            errorCode += 1
                            break
                        else:
                            new_sheet.cell(row=row-8, column=1, value=sheet.cell_value(row, 1))
                            new_sheet.cell(row=row-8, column=2, value=sheet.cell_value(row, 2))
                            new_sheet.cell(row=row-8, column=3, value=sheet.cell_value(row, 3))
                            new_sheet.cell(row=row-8, column=4, value=sheet.cell_value(row, 4))
                            new_sheet.cell(row=row-8, column=5, value=sheet.cell_value(row, 9))

                    # Lưu workbook mới thành tệp Excel
                    new_workbook.save('error.xls')

                    #Khởi tạo response
                    response = HttpResponse(content_type='application/vnd.ms-excel')
                    response['Content-Disposition'] = 'attachment; filename=error.xls'
                    new_workbook.save(response)
                    return response

            except Exception as e:
                # print track trace of exception
                print(traceback.format_exc())
                error = e.args[0]
                context['error'] = error

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
@require_http_methods(['POST'])
def export(request):
    if request.method == 'POST':
        html = request.POST.get('html','')
        
        # Phân tích cú pháp html và trích xuất du liệu
        soup = BeautifulSoup(html, 'html.parser')
        table = soup.find('table')
        rows = table.find_all('tr')
        data = []
        th=[]
        #  Lấy tiêu đề cột
        titles = rows[0].find_all('th')
        for title in titles:
            if title.find('div', class_='get-text'): # Bỏ qua thẻ <th> không có class 'get-text'
                th.append(title.find('div', class_='get-text').text.strip())
        data.append(th)

        for row in rows:
            td=[]
            if row.find('th'): # Bỏ qua thẻ <th>
                continue
            cols = row.find_all('td')
            for col in cols:
                if col.find('input', type='checkbox'):# Kiểm tra thẻ có input type='checkbox' không
                    if col.find('input', type='checkbox').get('checked'):
                        td.append('X')
                    else:
                        td.append('O')
                else:
                    td.append(col.text.strip() if col else '')
            data.append(td)
        # Tạo workbook mới
        wb = openpyxl.Workbook()
        # Lấy sheet đầu tiên
        sheet = wb.active
        
        # Ghi chú
        sheet['A1'] = 'Học viện nông nghiệp Việt Nam'
        #merge cell A1 đen G1
        sheet.merge_cells('A1:G1')
        
        # Ghi chú
        sheet['A2'] = 'Thông tin điểm danh sinh viên'
        
        # Thêm dư liệu vào sheet vào sheet bắt đầu từ dòng số 3
        for row in data:
            sheet.append(row)
            
        # vẽ border cho tất cả các cell từ 1 đến len(data)
        for row in sheet.iter_rows(min_row=1, max_row=len(data) + 2, min_col=1, max_col=len(data[0])):
           for cell in row:
                cell.border = openpyxl.styles.Border(left=openpyxl.styles.Side(border_style='thin', color='FF000000'), right=openpyxl.styles.Side(border_style='thin', color='FF000000'), top=openpyxl.styles.Side(border_style='thin', color='FF000000'), bottom=openpyxl.styles.Side(border_style='thin', color='FF000000'))
                # Auto dãn cột theo nội dung
                sheet.column_dimensions[get_column_letter(cell.column)].auto_size = True
            
        
        # Thiết lập các thuộc tính cho file Excel
        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = 'attachment; filename=myfile.xlsx'  
        
        # Lưu workbook vào response
        wb.save(response)
    return response

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def search_student(request):    
    if request.method == 'POST':
        student_code = request.POST.get('student_code','')
        class_code = request.POST.get('class_code','')
        response = Response(SUCCESS_CODE, 'Success', None)
        try:
            #Lấy ra danh sách sinh viên trong lớp
            studentInSubjectClass = StudentInSubjectClass.objects.filter(class_code=class_code)
            ids = list(studentInSubjectClass.values_list('student_code', flat=True))
            
            # Query lấy tối đa 20 kết quả đầu tiên dưa vào mã sinh viên
            students = Student.objects.filter(student_code__contains=student_code).exclude(auto_id__in=ids)[:20]
            response = Response(SUCCESS_CODE, 'Success', list(students.values()))
            return JsonResponse(response.to_dict(), safe=False)
        except Student.DoesNotExist:
            response = Response(ResponseStatus.NOT_FOUND, 'Not found', None)
        except Exception as e:
            response = Response(ERROR_CODE, str(e), None)
        return JsonResponse(response.to_dict(), safe=False)
    return JsonResponse(Response(ERROR_CODE, 'Method not allowed', None).to_dict(), safe=False)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def studentInClass(request):
    if request.method == 'POST':
        
        # Lấy mã lớp từ request
        class_code = request.POST.get('class_code','')
        student_code = request.POST.get('student_code','')
        
        #Lấy danh sách sinh viên trong lớp
        studentsInSubjectClass = StudentInSubjectClass.objects.filter(class_code=class_code)
        
        ids = list(studentsInSubjectClass.values_list('student_code', flat=True))

        #Lấy ra danh sách sinh viên
        students = Student.objects.filter(auto_id__in=ids)
        
        if student_code != '':
            students = students.filter(student_code__contains=student_code)
        
        response = Response(SUCCESS_CODE, 'Success', list(students.values()))
        return JsonResponse(response.to_dict(), safe=False)
    return JsonResponse(Response(ERROR_CODE, 'Method not allowed', None).to_dict(), safe=False)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def add_student_to_class(request):
    if request.method == 'POST':
        # Lấy mã lớp từ request
        class_code = request.POST.get('class_code','')
        student_code = request.POST.get('student_code','')
        
        print("class_code: " + class_code)
        print("student_code: " + student_code)
        response = Response(SUCCESS_CODE, 'Success', None)
        try:
            #Kiểm tra xem sinh viên đã tồn tại trong lớp chưa
            studentInSubjectClass = StudentInSubjectClass.objects.filter(class_code=class_code, student_code=student_code)
            if studentInSubjectClass.count() > 0:
                response = Response(ERROR_CODE, 'Sinh viên đã tồn tại trong lớp', None)
                return JsonResponse(response.to_dict(), safe=False)
            #Thêm sinh viên vào lớp
            studentInSubjectClass = StudentInSubjectClass(class_code=SubjectClass.objects.get(auto_id=class_code), student_code=Student.objects.get(auto_id=student_code))
            studentInSubjectClass.save()
            # Kiểm tra lịch sử điểm danh của lớp
            attendance = Attendance.objects.filter(subject_class=class_code)
            
            #Lấy ra những lần điểm danh của lớp học
            date = attendance.values_list('date_time', flat=True)     
            
            #Lấy ra những lần điểm danh duy nhất
            date = list(set(date))
            for d in date:
                #Kiểm tra tại mỗi lần d sinh viên đã có dữ liệu điểm danh chưa
                attendance = Attendance.objects.filter(subject_class=class_code, date_time=d, student_code=student_code)
                if attendance.count() == 0:
                    #Nếu chưa có thì thêm vào với giá trị mặc định là 0
                    Attendance(subject_class=SubjectClass.objects.get(auto_id=class_code), 
                               student_code=Student.objects.get(auto_id=student_code), date_time=d, 
                               created_by = request.user.username, is_present = '0').save()
            response = Response(SUCCESS_CODE, 'Success', None)
        except Exception as e:
            response = Response(ERROR_CODE, str(e), None)
        return JsonResponse(response.to_dict(), safe=False)
    return JsonResponse(Response(ERROR_CODE, 'Method not allowed', None).to_dict(), safe=False)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def delete_student_in_class(request):
    if request.method == 'POST':
        # Lấy mã lớp từ request
        class_code = request.POST.get('class_code','')
        student_code = request.POST.get('student_code','')
        response = Response(SUCCESS_CODE, 'Success', None)
        try:
            #Xóa sinh viên khỏi lớp
            studentInSubjectClass = StudentInSubjectClass.objects.filter(class_code=class_code, student_code=student_code)
            studentInSubjectClass.delete()
            response = Response(SUCCESS_CODE, 'Success', None)
        except Exception as e:
            response = Response(ERROR_CODE, str(e), None)
        return JsonResponse(response.to_dict(), safe=False)
    return JsonResponse(Response(ERROR_CODE, 'Method not allowed', None).to_dict(), safe=False)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def attendances(request):
    if request.method == 'POST':
        imgFile = request.FILES.get('image')
        class_code = request.POST.get('class_code','')
        if imgFile is not None:
            fs = FileSystemStorage(location="media/face/")
            filename = fs.save('imgs_request/' + imgFile.name, imgFile)
            #get path of image file
            path = fs.path(filename)
            
            # Danh sách sinh viên xuất hiện trong ảnh
            fileName,students = recognize_face(path)
            
            #Lấy ra lớp học
            subject_class = SubjectClass.objects.get(auto_id=class_code)
            
            # Lấy danh sách sinh viên trong lớp
            studentInSubjectClass = StudentInSubjectClass.objects.filter(class_code=class_code)
            
            #Lấy ra thời gian điểm danh
            now = datetime.now()
            
            #Danh sach điểm danh
            attendances = []
            
            urlImg = settings.MEDIA_ATTENDANCE_URL + fileName
            
            #Tạo thông tin điểm danh (đối chiếu danh sách sinh viên trong lớp với danh sách sinh viên xuất hiện trong ảnh)
            for studentInClass in studentInSubjectClass:
                if studentInClass.student_code in students:
                    attendance = Attendance(subject_class=subject_class, student_code=studentInClass.student_code, is_present='1', date_time=now, imagePath=urlImg)
                else:
                    attendance = Attendance(subject_class=subject_class, student_code=studentInClass.student_code, is_present='0', date_time=now, imagePath=urlImg)
                attendances.append(attendance.serializable())
            response = Response(SUCCESS_CODE, urlImg, attendances)
            return JsonResponse(response.to_dict(), safe=False)
        else:
            return JsonResponse(Response(ERROR_CODE, 'Không nhận được ảnh với key image', None).to_dict(), safe=False)
    return JsonResponse(Response(ERROR_CODE, 'Method not allowed', None).to_dict(), safe=False)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
@require_http_methods(["POST"])
def saveAttendance(request):
    if request.method == 'POST':
        attendances = request.POST.get('attendances','')
        attendances = json.loads(attendances)
        lst = []
        now = datetime.now()
        for attendance in attendances:
            student = Student.objects.get(auto_id=attendance['student_code']['auto_id'])
            subject_class = SubjectClass.objects.get(auto_id=attendance['subject_class']['auto_id'])
            _attendance = Attendance(student_code=student, subject_class=subject_class, is_present=attendance['is_present'], date_time=now, imagePath=attendance['imagePath'])
            _attendance.created_by = request.user.username
            lst.append(_attendance)
        bulk_create_attendance = Attendance.objects.bulk_create(lst)
        print(bulk_create_attendance)
        response = Response(SUCCESS_CODE, SUCCESS_MESSAGE, None)
        return JsonResponse(response.to_dict(), safe=False)
    return JsonResponse(Response(ERROR_CODE, 'Method not allowed', None).to_dict(), safe=False)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
@require_http_methods(["POST"])
def update_attendance(request):
    if request.method == 'POST':
        attendance_id = request.POST.get('attendance_id','')
        is_present = request.POST.get('is_present','')
        attendance = Attendance.objects.get(auto_id=attendance_id)
        attendance.is_present = is_present
        attendance.updated_by = request.user.username
        attendance.updated_at = datetime.now()
        attendance.save()
        response = Response(SUCCESS_CODE, SUCCESS_MESSAGE, None)
        return JsonResponse(response.to_dict(), safe=False)
    return JsonResponse(Response(ERROR_CODE, 'Method not allowed', None).to_dict(), safe=False)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
@require_http_methods(["DELETE"])
def delete_attendance(request):
    if request.method == 'DELETE':
        date_str = request.GET.get('datetime','')
        subjectClass = request.GET.get('subjectClass','')
        if date_str == '' or subjectClass == '':
            return JsonResponse(Response(ERROR_CODE, 'Thiếu thông tin datetime hoặc mã lớp', None).to_dict(), safe=False)
        date_time_obj = datetime.strptime(date_str, '%d/%m/%Y %H:%M:%S.%f')
        attendances = Attendance.objects.filter(date_time=date_time_obj, subject_class=subjectClass)
        attendances.delete()
        response = Response(SUCCESS_CODE, SUCCESS_MESSAGE, None)
        return JsonResponse(response.to_dict(), safe=False)
    return JsonResponse(Response(ERROR_CODE, 'Method not allowed', None).to_dict(), safe=False)

# @login_required(login_url='/login')
@user_passes_test(lambda u: u.is_active, login_url='/admin/login')
@require_http_methods(["POST"])
def statistical(request):
    if request.method == 'POST':
        auto_id = request.POST.get('subjectClass','')
        user = request.user
        
        if auto_id == '':
            return JsonResponse(Response(ERROR_CODE, 'Thiếu thông tin mã lớp', None).to_dict(), safe=False)
        
        #Lấy ra lớp học
        subject_class = SubjectClass.objects.get(auto_id=auto_id)
        if (user.is_staff or user.is_superuser) and user.username != subject_class.created_by:
            return JsonResponse(Response(ERROR_CODE, 'Bạn không có quyền xem lớp này', None).to_dict(), safe=False)

        #Lấy ra danh sách sinh viên trong lớp
        studentInSubjectClass = StudentInSubjectClass.objects.filter(class_code=subject_class)
        
        if not user.is_staff and not user.is_superuser and user.is_active:
            if user.username not in studentInSubjectClass.values_list('student_code__student_code', flat=True):
                return JsonResponse(Response(ERROR_CODE, 'Bạn không có quyền xem lớp này', None).to_dict(), safe=False)
        
        #Lấy ra danh sách điểm danh của lớp
        attendances = Attendance.objects.filter(subject_class=subject_class)
        
        #Lấy ra danh sách các ngày điểm danh
        dates = attendances.values('date_time').distinct()
        
        # Số lần điêm danh của lớp
        count = len(dates)
        
        # Tính % điểm danh của từng sinh viên
        lst = []
        for studentInClass in studentInSubjectClass:
            count_attendance = 0
            for attendance in attendances:
                if attendance.student_code == studentInClass.student_code:
                    if attendance.is_present == '1':
                        count_attendance += 1
            if count == 0:
                percent = 0
            else:
                percent = round(count_attendance / count * 100, 2)
            lst.append({'student_code': studentInClass.student_code.serializable(), 'percent': percent,'lose':count - count_attendance})
        response = Response(SUCCESS_CODE, SUCCESS_MESSAGE, lst)
        return JsonResponse(response.to_dict(), safe=False)
    return JsonResponse(Response(ERROR_CODE, 'Method not allowed', None).to_dict(), safe=False)
        
@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
@require_http_methods(["POST"])
def statistical_student_warning(request):
    if request.method == 'POST':
        auto_id = request.POST.get('subjectClass','')

        if auto_id == '':
            return JsonResponse(Response(ERROR_CODE, 'Thiếu thông tin mã lớp', None).to_dict(), safe=False)
        
        #Lấy ra lớp học
        subject_class = SubjectClass.objects.get(auto_id=auto_id)
        
        #Lấy ra danh sách sinh viên trong lớp
        studentInSubjectClass = StudentInSubjectClass.objects.filter(class_code=subject_class)
        
        #Lấy ra danh sách điểm danh của lớp
        attendances = Attendance.objects.filter(subject_class=subject_class)
        
        #Lấy ra danh sách các ngày điểm danh
        dates = attendances.values('date_time').distinct()
        
        # Số lần điêm danh của lớp
        count = len(dates)
        
        # Tính % điểm danh của từng sinh viên
        lst = []
        for studentInClass in studentInSubjectClass:
            count_attendance = 0
            for attendance in attendances:
                if attendance.student_code == studentInClass.student_code:
                    if attendance.is_present == '1':
                        count_attendance += 1
            percent = round(count_attendance / count * 100, 2)
            lst.append({'student_code': studentInClass.student_code.student_code, 'percent': percent})
        response = Response(SUCCESS_CODE, SUCCESS_MESSAGE, lst)
        return JsonResponse(response.to_dict(), safe=False)
    return JsonResponse(Response(ERROR_CODE, 'Method not allowed', None).to_dict(), safe=False)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
@require_http_methods(["POST"])
def send_email_api(request):
    if request.method == 'POST':
        recipient = request.POST['recipient']
        subject = request.POST['subject']
        message = request.POST['message']
        sender = settings.EMAIL_HOST_USER
        recipient = recipient.split(' ')
        
        # Nội dung HTML từ template
        html_content = message

        # Tạo phiên bản văn bản thuần túy của email (không chứa thẻ HTML)
        text_content = strip_tags(html_content)
        
        # Tạo đối tượng EmailMultiAlternatives và thiết lập nội dung
        email = EmailMultiAlternatives(
            subject=subject,
            body=text_content,
            from_email=sender,
            to=recipient
        )
        
        email.attach_alternative(html_content, "text/html")
        status = email.send()
        # status = send_mail(subject, message, sender, recipient)
        if status == 1:
            return JsonResponse(Response(SUCCESS_CODE, SUCCESS_MESSAGE, None).to_dict(), safe=False)
    return JsonResponse(Response(ERROR_CODE, 'Method not allowed', None).to_dict(), safe=False)

# LEARN
@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def learns_create(request, class_code):
    if request.method == 'POST':
        if request.POST['auto_id'] == '':
            learns = Learning()
            learns.subject_class = SubjectClass.objects.get(auto_id=int(class_code))
            learns.title = request.POST['title']
            learns.content = request.POST['content']
            learns.created_by = request.user.username
            learns.save()
            if 'file' in request.FILES:
                files = request.FILES.getlist('file')
                # Lưu file vào thư mục media/documents
                fs = FileSystemStorage(location='media/documents')
                for file in files:
                    filename = fs.save(file.name, file)
                    path = '/documents/' + filename
                    PathFileLearning(learning = learns, path = path, name = filename, created_by = learns.created_by).save()
        else:
            learns = Learning.objects.get(auto_id=int(request.POST['auto_id']))
            learns.title = request.POST['title']
            learns.content = request.POST['content']
            learns.updated_by = request.user.username
            learns.save()
            if 'file' in request.FILES:
                files = request.FILES.getlist('file')
                # Lưu file vào thư mục media/documents
                fs = FileSystemStorage(location='media/documents')
                for file in files:
                    filename = fs.save(file.name, file)
                    path = '/documents/' + filename
                    PathFileLearning(learning = learns, path = path, name = filename, created_by = learns.created_by).save()
        return JsonResponse(Response(SUCCESS_CODE, SUCCESS_MESSAGE, None).to_dict(), safe=False)
    return JsonResponse(Response(ERROR_CODE, 'Method not allowed', None).to_dict(), safe=False)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def learns_delete(request, auto_id):
    if request.method == 'POST':
        Learning.objects.filter(auto_id=int(auto_id)).delete()
        return JsonResponse(Response(SUCCESS_CODE, SUCCESS_MESSAGE, None).to_dict(), safe=False)
    return JsonResponse(Response(ERROR_CODE, 'Method not allowed', None).to_dict(), safe=False)

@user_passes_test(lambda u: u.is_staff, login_url='/admin/login')
def deleteDocument(request, auto_id):
    if request.method == 'DELETE':
        
        pathFileLearning = PathFileLearning.objects.get(auto_id=int(auto_id))
        fileNames = pathFileLearning.name
        # Xoa file trong media/documents
        fs = FileSystemStorage(location='media/documents')
        fs.delete(fileNames)
        
        pathFileLearning.delete()
        return JsonResponse(Response(SUCCESS_CODE, SUCCESS_MESSAGE, None).to_dict(), safe=False)
    return JsonResponse(Response(ERROR_CODE, 'Method not allowed', None).to_dict(), safe=False)