import shutil
from app.models import AttendanceTimeAndImg
from face.models import *
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import user_passes_test, login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.views.decorators.csrf import csrf_protect
from django.core.paginator import Paginator
from django.core.files.storage import FileSystemStorage
from face.common.common import *
from django.core.files import File

# Create your views here.
@csrf_protect
def login_student(request):
    requestClient = {}
    if request.method == 'POST':
        username = request.POST['email']
        password = request.POST['password']
        requestClient = {"username": username, "password": password}
        user = authenticate(request, username=username, password=password)
        if user is not None and user.is_active:
            print(user.is_staff)
            if user.is_staff is not True and user.is_superuser is not True:
                login(request, user)
                return redirect('home_student')
            else:
                messages.error(request, 'Tài khoản không có quyền truy cập chức năng này.')
        else:
            messages.error(request, 'Tài khoản hoặc mật khẩu không chính xác.')
        
    return render(request, 'login.html', requestClient)

@user_passes_test(lambda u: not u.is_staff and not u.is_superuser, login_url='/login')
def logout_student(request):
    logout(request)
    return redirect('login_student')

@user_passes_test(lambda u: not u.is_staff and not u.is_superuser, login_url='/login')
def index(request):
    # Lấy user hiện tại
    user = request.user
        
    # Lấy thông tin sinh viên
    student_code = user.username

    #Kiểm tra xem sinh viên có tồn tại trong hệ thống hay không
    student = Student.objects.filter(student_code=student_code).first()
    
    if student is None:
        messages.error(request, 'Sinh viên không tồn tại trong hệ thống.')
        return redirect('login_student')
    #Lấy danh sách ảnh sinh viên
    studentImages = StudentImages.objects.filter(student_code=student.auto_id)
    context = {'student': student, 'studentImages': studentImages}
    return render(request, 'index.html', context)

@user_passes_test(lambda u: not u.is_staff and not u.is_superuser, login_url='/login')
def student_update(request):
    student_code = request.user.username
    student = Student.objects.get(student_code=student_code)
    if request.method == 'POST':
        student.phone = request.POST['phone']
        student.email = request.POST['email']
        student.address = request.POST['address']
        if 'avatar' in request.FILES:
            avatar = request.FILES['avatar']
            student.avatar = avatar
        student.save()
        return redirect('home_student')
    context = {'student': student}
    return render(request, 'update.html',context)

@user_passes_test(lambda u: not u.is_staff and not u.is_superuser, login_url='/login')
def images_student(request):
    student_code = request.user.username
    errors = None
    student_obj = Student.objects.get(student_code=student_code)
    studentID = student_obj.auto_id
    if request.method == 'POST':
        #Kiem tra xem sinh vien da du 100 anh tren he thong chua
        totalImageCurrent = StudentImages.objects.filter(student_code=studentID).count()
        if totalImageCurrent < 100:
            images = request.FILES.getlist('images') # getlist() returns a list of files
            # Lay ra so luong anh da duoc upload
            totalImages = images.__len__()
            student = Student.objects.get(student_code=student_code)
            if totalImages + totalImageCurrent <= 100:
                for image in images:
                    StudentImages.objects.create(student_code=student, images=image)
            else:
                errors = []
                errors.append('Mỗi sinh viên chỉ được upload tối đa 100 ảnh trên hệ thống!')
                errors.append('Bạn đã upload ' + str(totalImageCurrent) + ' ảnh, chỉ được thêm ' + str(100 - totalImageCurrent) + ' ảnh nữa!')
                errors.append('Vui lòng xóa bớt ảnh cũ trước khi upload ảnh mới hoặc giảm số lượng ảnh upload!')
        else:
                errors = []
                errors.append('Sinh viên này đã upload đủ 100 ảnh trên hệ thống, không thể upload thêm ảnh nữa!')
            
    studentImages = StudentImages.objects.filter(student_code=studentID).order_by('-created_at')
    context = {'studentImages': studentImages,"errors":errors,"student":student_obj}
    return render(request, 'image.html',context)

@user_passes_test(lambda u: not u.is_staff and not u.is_superuser, login_url='/login')
def video(request):
    if request.method == 'POST':
        student_code = request.user.username
        student = Student.objects.get(student_code=student_code)
        video = request.FILES['video']
        if video is not None:
            
            #check file video is video
            if video.name.endswith('.mp4') or video.name.endswith('.avi') or video.name.endswith('.mov'):
                #check file video is not empty
                if video.size > 0:
                    #check file video is not larger than 50MB
                    if video.size < 52428800:
                        print(video.size)
                    else:
                        print('Video quá lớn, vui lòng chọn video nhỏ hơn 50MB!')
                else:
                    messages.error(request, 'Video không được để trống!')
            
            # Lưu video lại
            fs = FileSystemStorage(location='media/face/')
            
            # Lưu video vào thư mục media/videos
            vs = fs.save('videos/' + video.name, video)
            
            # Lấy đường dẫn tương đối của video
            pathVideo = 'media/face/videos/' + video.name

            pathImgs = capture_frames(pathVideo, 3)
            for path in pathImgs:
                with open(path, 'rb') as img_file:
                    img_field = File(img_file)
                    StudentImages.objects.create(student_code=student, images=img_field)
                    
            # Xóa video trong thư mục media/videos
            fs.delete('videos/' + video.name)
            
            # Xóa folder temp
            shutil.rmtree('media/face/temp')
            #Tạo folder temp mới
            os.mkdir('media/face/temp')
    return redirect('images_student')

@user_passes_test(lambda u: not u.is_staff and not u.is_superuser, login_url='/login')
def delete_image_student(request):
    if request.method == 'POST':
        auto_id = request.POST['auto_id']
        StudentImages.objects.get(auto_id=auto_id).delete()
    return redirect('images_student')

@user_passes_test(lambda u: not u.is_staff and not u.is_superuser, login_url='/login')
def classes(request):
    class_code = request.GET.get('class_code','')
    class_name = request.GET.get('class_name','')
    hoc_ky = request.GET.get('hoc_ky','')
    nam_hoc = request.GET.get('nam_hoc','')
    
    user = request.user
    
    #Lấy ra danh sách lớp của sinh viên đó
    student_code = user.username
    student = Student.objects.get(student_code=student_code)
    
    studentInSubjectClass = StudentInSubjectClass.objects.filter(student_code=student.auto_id)
    subjectClass = SubjectClass.objects.filter(auto_id__in=studentInSubjectClass.values('class_code')).order_by('-created_at')

    
    if class_code != '':
        subjectClass = subjectClass.filter(class_code__icontains=class_code)
    if class_name != '':
        subjectClass = subjectClass.filter(class_name__icontains=class_name)
    if hoc_ky != '':
        subjectClass = subjectClass.filter(hoc_ky__icontains=hoc_ky)
    if nam_hoc != '':
        subjectClass = subjectClass.filter(nam_hoc__icontains=nam_hoc)
    student = Student.objects.get(student_code=student_code)
            
    paginator = Paginator(subjectClass, 10) # Mỗi trang chứa 10 item
    page = request.GET.get('page')
    subjectClass = paginator.get_page(page)
    context = {"user": request.user, "subjectClass": subjectClass,"student":student}
    return render(request, 'class.html', context)

@user_passes_test(lambda u: not u.is_staff and not u.is_superuser, login_url='/login')
def class_attendance(request, class_code):

    # Lấy thông tin lớp học
    subjectClass = SubjectClass.objects.get(class_code=class_code)

    #Lấy danh sách sinh viên trong lớp
    studentInSubjectClass = StudentInSubjectClass.objects.filter(class_code=subjectClass)
    
    #Lấy ra các lần điểm danh
    attendances = Attendance.objects.filter(subject_class=subjectClass).order_by('date_time')
    
    attendanceTimeAndImg = []
    attendanceTimeAndImgTemp = []
    
    #Lấy ra danh sách thời gian điểm danh group by theo date_time
    for item in attendances:
        if item.date_time not in attendanceTimeAndImgTemp:
            attendanceTimeAndImgTemp.append(item.date_time)
            attendanceTimeAndImg.append(AttendanceTimeAndImg(item.date_time, item.imagePath, item.created_by))
            
    #Lấy ra note các lần học của lớp học
    learns = Learning.objects.filter(subject_class=subjectClass)
    pathFileLearnings = PathFileLearning.objects.filter(learning__in=learns)
    student = Student.objects.get(student_code=request.user.username)
    
    context = {"attendances": attendances,
               "subjectClass": subjectClass,
               "studentInSubjectClass": studentInSubjectClass,
               "attendanceTimeAndImgs": attendanceTimeAndImg,
                "learns": learns,
                "pathFileLearnings": pathFileLearnings,
                "student":student
               }
    
    return render(request, 'hoc_tap_va_diem_danh.html',context)
