from django.urls import path
from student import views as app_views
from face import views as face_views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('login', app_views.login_student, name='login_student'),
    path('logout', app_views.logout_student, name='logout_student'),
    path('', app_views.index, name='home_student'),
    path('update', app_views.student_update, name='update_student'),
    path('photos', app_views.images_student, name='images_student'),
    path('video', app_views.video, name='video_student'),
    path('photos-delete', app_views.delete_image_student, name='delete_image_student'),
    path('classes', app_views.classes, name='classes_student'),
    path('classes-attendance/<str:class_code>', app_views.class_attendance, name='classes_attendance_student'),
    path('doi-mat-khau/<str:token>', face_views.datLaiMatKhau, name='dat_lai_mat_khau'),
]
# urlpatterns += static(settings.MEDIA_FACE_URL, document_root=settings.MEDIA_FACE)
