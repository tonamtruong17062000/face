from datetime import datetime, timedelta
from django.conf import settings
from django.utils import timezone
import json
from django.core.serializers.json import DjangoJSONEncoder


class SessionTimeoutMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.is_authenticated:
            last_activity_str = request.session.get('last_activity') # Lấy thời gian hoạt động gần nhất của user
            if last_activity_str:
                last_activity = datetime.strptime(last_activity_str.strip('"'), '%Y-%m-%d %H:%M:%S') # Chuyển đổi chuỗi thành datetime
                last_activity = timezone.make_aware(last_activity, timezone.utc) # Chuyển đổi datetime thành datetime có múi giờ
                now = timezone.now()
                if now > last_activity + timedelta(seconds=settings.SESSION_COOKIE_AGE): # Nếu thời gian hiện tại lớn hơn thời gian hoạt động gần nhất + thời gian timeout
                    request.session.flush() # Xóa session
                    return self.get_response(request) # Trả về response
            json_serializable_datetime = timezone.now().strftime('%Y-%m-%d %H:%M:%S') # Chuyển đổi thời gian hiện tại thành chuỗi có thể chuyển đổi thành json
            request.session['last_activity'] = json.dumps(json_serializable_datetime, cls=DjangoJSONEncoder) # Cập nhật thời gian hoạt động gần nhất của user
        response = self.get_response(request)
        return response
