"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

# load images
from django.conf import settings
from django.conf.urls.static import static
from error.views import page_not_found, server_error

urlpatterns = [
    path('superadmin/', admin.site.urls),
    path('api/', include('app.urls')),
    path('admin/', include('face.urls')),
    # path('error/', include('error.urls')),
    path('', include('student.urls')),
]
admin.site.site_header = "Face VNUA"
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.MEDIA_ATTENDANCE_URL, document_root=settings.MEDIA_ATTENDANCE)
urlpatterns += static(settings.MEDIA_VIDEO_URL, document_root=settings.MEDIA_VIDEO)
urlpatterns += static(settings.MEDIA_DOCUMENT_URL, document_root=settings.MEDIA_DOCUMENT)
urlpatterns += static(settings.MEDIA_TEACHER_URL, document_root=settings.MEDIA_TEACHER)

# Error handlers
handler404 = 'error.views.page_not_found'
handler500 = 'error.views.server_error'