from django.urls import path
from . import views

urlpatterns = [
    path('404/', views.page_not_found, name='404'),
    path('500/', views.server_error, name='500'),
]