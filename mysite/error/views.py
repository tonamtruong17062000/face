from django.shortcuts import render


def page_not_found(request, exception):
    return render(request, '404.html', status=404)

def server_error(request, *args, **kwargs):
    return render(request, '500.html', status=500)

def expired(request):
    return render(request, '404.html')